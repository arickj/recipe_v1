﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;

namespace RecipeViewer
{
    class Startup
    {
        [System.STAThreadAttribute()]
        static void Main()
        {
            try
            {
                RecipeViewer.App app = new RecipeViewer.App();
                app.InitializeComponent();
                app.Run();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
