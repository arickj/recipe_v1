﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using RecipesEDM;

namespace RecipeViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public static string ErrorBoxMessage;
        public static Exception TopErrorException;

        private bool validWindowClose = false;

        public MainWindow()
        {
            InitializeComponent();
            try
            {
                using (RecipesContext context = new RecipesContext())
                {
                    LoadXML(context);
                }
            }
            catch (NullReferenceException ex)
            {
                SetTopExceptionError(ex, "The database failed to load correctly.");
            }

            if (ErrorBoxMessage != null)
            {
                SetMessageBox(ErrorBoxMessage);
            }
            else
            {
                SetMessageBox("The database has been loaded correctly.", true);
            }
        }

        public void SetMessageBox(string message, bool positive = false)
        {
            if (!positive)
                textBlockNotifications.Foreground = Brushes.Red;
            else
                textBlockNotifications.Foreground = Brushes.Green;
            textBlockNotifications.Text = message;
        }

        public static void SetTopExceptionError(Exception ex, string message)
        {
            if (TopErrorException == null)
            {
                TopErrorException = ex;
                ErrorBoxMessage = message;
            }
        }

        public void LoadXML(RecipesContext context)
        {
            listBox_Recipes.DataContext = (
                from r in context.Recipes
                select r).Include(r => r.Ingredients).ToList();
        }

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBox_Recipes.SelectedIndex >= 0)
            {
                Recipe recipe = (Recipe)listBox_Recipes.SelectedItem;
                textBox_RecipeType.Text = "• Recipe Type  :  " + recipe.RecipeType
                                            + "\n• Yield  :  " + recipe.Yield
                                            + "\n\n• Serving Size  :\n" + recipe.ServingSize;
                textBox_Directions.Text = recipe.Directions.Trim();
                textBox_Comments.Text = recipe.Comment?.Trim();

                StringBuilder ing_txt = new StringBuilder();
                foreach (var ing in recipe.Ingredients)
                {
                    ing_txt.Append(ing.Description).Append('\n');
                }
                textBox_Ingredients.Text = ing_txt.ToString();
            }
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                validWindowClose = true;
                this.Close();
            }
            finally
            {
                validWindowClose = false;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!validWindowClose)
            {
                e.Cancel = true;
                CloseDialogBox dialog = new CloseDialogBox();
                #region CloseDialogBox Initialization
                dialog.Title = this.Title;
                dialog.Left = this.Left + (this.Width / 2) - (dialog.Width / 2);
                dialog.Top = this.Top + (this.Height / 2) - (dialog.Height / 2);
                dialog.ShowDialog();
                #endregion
            }
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            using (RecipesContext context = new RecipesContext())
            {
                listBox_Recipes.DataContext = (
                           from r in context.Recipes
                           select r).Include(r => r.Ingredients).ToList();
                listBox_Recipes.SelectedIndex = -1;
                lblNotifications.Visibility = Visibility.Hidden;
                textBlockNotifications.Visibility = Visibility.Hidden;
                textBox_RecipeType.Text = textBox_Directions.Text = textBox_Ingredients.Text = textBox_Comments.Text = string.Empty;
            }

        }
    }
}
